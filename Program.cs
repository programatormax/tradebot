﻿using System.Net.Http.Headers;
using var client = new HttpClient();
  
  Random rnd = new Random();
     string url1 = "https://api.tdameritrade.com/v1/oauth2/token";
     string url2 = "https://api.tdameritrade.com/v1/oauth2/token";
     int val  = rnd.Next(10000, 65535);
     var uid = "id"+val.ToString();
     Console.WriteLine("[ MS ] Send autorization request on "+ url1);
     HttpResponseMessage response = await client.GetAsync(url1+"/token?uid="+uid);
     var ucode = await response.Content.ReadAsStringAsync();
     var utext = "hello";
     if (response.IsSuccessStatusCode)
     {
       Console.WriteLine("[ OK ] Received autorization code from server "+ url1+" ucode="+ ucode);
       Console.WriteLine("[ MS ] Send message with RIGHT autorization code on server "+ url2);
       HttpResponseMessage lresponse = await client.GetAsync(url2+"/send?uid="+uid+"&ucode="+ucode+"&utext="+utext);
       if (lresponse.IsSuccessStatusCode)
       {
         Console.WriteLine("[ OK ] Message was send");
       }
       else
       {
         Console.WriteLine("[ ER ] Message not sent");    
       }

       Console.WriteLine("[ MS ] Send message with WRONG autorization code on server "+url2);
       HttpResponseMessage nresponse = await client.GetAsync(url2+"/send?uid="+uid+"&ucode=blablabla&utext="+utext);
       if (nresponse.IsSuccessStatusCode)
       {
         Console.WriteLine("[ OK ] Message was send");
       }
       else
       {
         Console.WriteLine("[ ER ] Message not sent");    
       }

     }
     else
    {
      Console.WriteLine("[ ER ] Autorization fault on server "+url1);
    }
